import pl.imiajd.radziejewski.*;
public class TestOsoba
{
    public static void main(String[] args)
    {
        Student a = new Student("320","320",320);
        System.out.println(a.toString());
        //a = b;                 --- powoduje błąd kompilacji  (dlaczego ?)
        //a = (NazwanyPunkt) b;     --- powoduje błąd wykonania   (dlaczego ?)

        //a = c;                 --- powoduje błąd kompilacji  (dlaczego ?)
    }
}
