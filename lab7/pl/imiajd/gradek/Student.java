package pl.imiajd.gradek;
public class Student extends Osoba{
    private String kierunek;

    public Student(String kierunek,String nazwisko, int rokUrodzenia){
        super(nazwisko,rokUrodzenia);
        this.kierunek = kierunek;
    }
    public String toString(){
        String nala = "";
        nala += super.toString();
        nala +=  " ";
        nala += this.kierunek;
        return nala;
    }
}

