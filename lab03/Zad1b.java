import java.util.*;

public class Zad1b
{
    public static void main(String[] args)
    {
        System.out.println("Podaj n z zakresu <1,100>");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if(n < 1 || n > 100)
        {
            System.out.println("N nie miesci sie w zakresie");
            System.exit(0);
        }
        int tablica[] = new int[n];
        Random r = new Random();
        for(int j = 0 ; j < tablica.length ;j++){
            tablica[j] = r.nextInt(2000) - 1000;
        }
        for(int i = 0 ; i < tablica.length; i++){
            System.out.print(tablica[i] + " ");
        }
        System.out.println("");
        int ujemne = 0;
        int dodatnie = 0;
        int zerowe = 0;
        for(int j : tablica){
            if(j<0){
                ujemne++;
            }
            else if(j > 0){
                dodatnie++;
            }
            else{
                zerowe++;
            }

        }
        System.out.println("Ujemne: " + ujemne);
        System.out.println("Dodatnie: " + dodatnie);
        System.out.println("Zerowe: " + zerowe);



    }

}

