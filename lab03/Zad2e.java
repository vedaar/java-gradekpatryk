import java.util.*;

public class Zad2e
{
    public static void main(String[] args)
    {
        System.out.println("Podaj n z zakresu <1,100>");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if(n < 1 || n > 100)
        {
            System.out.println("N nie miesci sie w zakresie");
            System.exit(0);
        }
        int tablica[] = new int[n];
        generuj(tablica,n,-999,999);
        for(int i = 0 ; i < tablica.length; i++){
            System.out.print(tablica[i] + " ");
        }
        System.out.println("");
        System.out.println("Maks : " + dlugoscMaksymalnegoCiaguDodatnich(tablica));

    }
    public static void generuj(int tab[],int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        for(int i = 0 ; i < n; i++){
            tab[i] = r.nextInt(maxWartosc-minWartosc) + minWartosc;
        }
    }

    public static int ileDodatnich(int tab[]){
        int dodatnie = 0;
        for(int element: tab){
            if(element > 0){
                dodatnie++;
            }
        }
        return dodatnie;
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[]){
        int maks = 0;
        int temp = 0;
        for(int element: tab){
            if(element > 0){
                maks++;
            }
            if(maks > temp){
                temp = maks;
            }
            if(element < 0){
                maks = 0;
            }
        }
        return temp;
    }


}

